
## Getting Started

### Installation

First of all, install the dependencies to run this project.

- [NodeJS](http://nodejs.org/)
- [GulpJS](http://gulpjs.com/)


```sh
# Clone this repository
$ git clone https://erickcouto@bitbucket.org/erickcouto/getninjas.git new_project
$ cd new_project

# install dependencies
$ npm install
```


### Tasks

- `gulp`: Initialize watch for changes and a server(localhost:3000)
- `gulp build`: build the project

