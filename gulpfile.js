'use strict';

var gulp        = require('gulp');
var browserSync = require('browser-sync').create();
var reload      = browserSync.reload;
var sass        = require('gulp-sass');
var runSequence = require('run-sequence');
var imagemin    = require('gulp-imagemin');
var concat      = require('gulp-concat');
var autoprefixer = require('gulp-autoprefixer');


gulp.task('browserSync', function() {
  browserSync.init({
    server: {
      baseDir: './dist'
    },
  })
});


// Copy html and json files
gulp.task('copy', function () {
    gulp.src(['./src/index.html','./src/fields.json'])
        .pipe(gulp.dest('./dist/'));
});

// Concat js files
gulp.task('scripts', function() {
  return gulp.src('./src/js/*.js')
    .pipe(concat('main.js'))
    .pipe(gulp.dest('./dist/js/'));
});

// Compile sass file to css
gulp.task('sass', function() {
  return gulp.src('./src/sass/**/*.scss')
    .pipe(sass())
    .pipe(autoprefixer({
      browsers: ['last 2 versions'],
      cascade: false
    }))
    .pipe(gulp.dest('./dist/css'))
    .pipe(browserSync.reload({
      stream: true
    }))
});

// Compress images
gulp.task('images', function() {
	gulp.src(['./src/images/*.jpg', './src/images/*.png'])
		.pipe(imagemin())
		.pipe(gulp.dest('./dist/images'))
		.pipe(browserSync.reload({
	      stream: true
	    }))
});

// Watch files
gulp.task('watch', function(){
  gulp.watch('./src/sass/**/*.scss', ['sass']); 
  gulp.watch('./src/js/**/*.js', ['scripts']); 
  gulp.watch('./src/**/*', ['copy']); 
  gulp.watch(['./src/images/*.jpg', './src/images/*.png'], ['images']); 
  gulp.watch('./src/index.html', browserSync.reload); 
  gulp.watch('./src/js/**/*.js', browserSync.reload); 
})

// Gulp default
gulp.task('default', function (callback) {
  runSequence(['sass', 'images', 'scripts', 'copy', 'browserSync', 'watch'],
    callback
  )
})

//Gulp build
gulp.task('build', function (callback) {
  runSequence(['sass', 'images', 'scripts', 'copy'],
    callback
  )
})