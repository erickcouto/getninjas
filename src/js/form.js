var xhr = new XMLHttpRequest(),
    method = "GET",
    url = "fields.json";

xhr.open(method, url, true);
xhr.onreadystatechange = function () {
  if(xhr.readyState === XMLHttpRequest.DONE && xhr.status === 200) {

    var json = JSON.parse(xhr.responseText)

    console.log(json._embedded.request_fields);


    // Build Form step 1 =====


    var requestFields = json._embedded.request_fields;
    var html          = '';

    if (requestFields[0].type == 'enumerable' &&  requestFields[0].allow_multiple_value == true) {
      var type = 'checkbox';
      console.log(type)
    }

    for (var key in requestFields) {

      if (requestFields.hasOwnProperty(key)) {

          html += '<div id="box'+ [key] +'" class="m-form__box">';
          html += '<label class="m-form__label" for='+JSON.stringify(requestFields[key].label)+'>'+requestFields[key].label+'</label>';
          html += '</div>';
      }
    }


   // checkbox services

    var checkService = json._embedded.request_fields[0].values;

    var labelService = '';

    for (var key in checkService) {

      if (checkService.hasOwnProperty(key)) {

        labelService += '<li class="m-form__items">'+'<input class="m-form__checkbox" name='+JSON.stringify(requestFields[0].name)+' type='+type+'>'+'<label class="m-form__label m-form__label--check">'+checkService[key]+'</label>'+'</li>';

      }
    }

    // checkbox for Whom

    var checkWho = json._embedded.request_fields[1].values;

    var labelWho = '';

    for (var key in checkWho) {

      if (checkWho.hasOwnProperty(key)) {

        labelWho += '<li class="m-form__items">'+'<input class="m-form__checkbox" type='+type+'>'+'<label class="m-form__label m-form__label--check">'+checkWho[key]+'</label>'+'</li>';

      }
    }

    // select how many

    var selectMany = json._embedded.request_fields[2].values;

    var labelMany = '';

    for (var key in selectMany) {

      if (selectMany.hasOwnProperty(key)) {

        labelMany += '<option value='+selectMany[key]+'>'+selectMany[key]+'</option>';

      }
    }

    // Select when

    var selectWhen = json._embedded.request_fields[3].values;

    var labelWhen = '';

    for (var key in selectWhen) {

      if (selectWhen.hasOwnProperty(key)) {

        labelWhen += '<option value='+selectWhen[key]+'>'+selectWhen[key]+'</option>';

      }
    }


    document.getElementById('inputs').innerHTML += html;
    document.getElementById('box0').innerHTML += '<ul id="checkbox" class="m-form__checks">'+labelService+'</ul>'+'<span id="service-error" class="m-error"></span>';
    document.getElementById('box1').innerHTML += '<ul id="checkbox" class="m-form__checks">'+labelWho+'</ul>';
    document.getElementById('box2').innerHTML += '<select class="m-form__select">'+'<option value>'+"Indique o número de pessoas"+'</option>'+labelMany+'</select>';
    document.getElementById('box3').innerHTML += '<select id="select" class="m-form__select" name='+JSON.stringify(requestFields[3].name)+' required='+requestFields[3].required+'>'+'<option value="0">'+"Indique o prazo do serviço"+'</option>'+labelWhen+'</select>';
    document.getElementById('box4').innerHTML += '<textarea class="m-form__textarea" placeholder='+JSON.stringify(requestFields[4].placeholder)+' required="true"></textarea>';;


      
    // Build Form step 2 =====

    var user_fields = json._embedded.user_fields;
    var user        = '';


    for (var key in user_fields) {

      if (user_fields.hasOwnProperty(key)) {

          user += '<div id="box_user'+ [key] +'" class="m-form__box m-form__box--user">';
          user += '<label class="m-form__label" for='+JSON.stringify(user_fields[key].label)+'>'+user_fields[key].label+'</label>';
          user += '<input id="input" class="m-form__user" type='+JSON.stringify(user_fields[key].type)+' name='+JSON.stringify(user_fields[key].name)+' placeholder='+JSON.stringify(user_fields[key].placeholder)+' required='+JSON.stringify(user_fields[key].required)+'>';
          user += '</div>';
      }
    }

    document.getElementById('inputs-user').innerHTML += user;

  }
};
xhr.send();




// validate required form

var buttonOne = document.getElementById("btn-step-1");
var buttonTwo = document.getElementById("btn-step-2");
var formOne   = document.getElementById("inputs");
var formTwo   = document.getElementById("inputs-user");




buttonOne.addEventListener("click",function(e){
    validateOne();

},false);

buttonTwo.addEventListener("click",function(e){
    validateTwo();

},false);

function nextStep() {
  buttonOne.className += " s-display--hide";
  buttonTwo.className = " m-btn s-btn--hover";

  formOne.className += " s-display--hide";
  formTwo.className = " m-form__inputs m-form__inputs--user";
}

function validateOne() {

    // checkbox

    var checkboxs    = document.getElementsByName("Qual será o serviço?");
    var select       = document.getElementById("select").hasAttribute("required");;
    var inputs       = document.getElementById("select").hasAttribute("required");
    var errorService = document.getElementById("service-error");

    var okay = false;
    for(var i=0,l=checkboxs.length;i<l;i++)
    {
        if(checkboxs[i].checked)
        {
            okay=true;
            break;
        }
    }

    if(okay){

      nextStep();

    }else {

      errorService.innerHTML = 'Marque pelo menos uma opção.'; 
    }

}

// validate step 2

function validateTwo() {

    var validateElements = document.getElementsByClassName("m-form__user");    
    
    
    var inputs = Array.prototype.filter.call(validateElements,         function(element){
        return element.nodeName === 'INPUT';
    });
    

    
    for(var i=0; i < inputs.length; i ++ ){
        var input = inputs[i];

        if(input.value.length == 0){

            var d     = document.getElementsByTagName("span")[0];
            input.placeholder = "Campo requerido.";

            input.style.borderColor = "red";

        }
    }

}